VIM customization
=================

different cursor shapes for different VIM modes
-----------------------------------------------

* Vertical line between characters in INSERT mode
* Box over a character in REPLACE mode
* Underline in NORMAL mode.

Blinking underline cursor is inspired by nostalgic hardware-generated cursor on PDP-11.

:set number
-----------

I love it to be in darkgray, so explicit color is added _after_ colorscheme is applied.

taglist
-------

Shows all the tags from opened buffers in separate vertical window. Toggle by <F8>.

Installation:
```
$ mkdir -p $HOME/.vim/pack/downloads/start/taglist
$ cd $HOME/.vim/pack/downloads/start
$ git clone https://github.com/yegappan/taglist
```

Homepage:
https://github.com/yegappan/taglist

Deps:
need ctags to work

ALE
---

Similar to emacs `flymake`. Runs syntax check on the current file, show warnings and errors on the fly.
 
Installation:
```
mkdir -p ~/.vim/pack/git-plugins/start
git clone --depth 1 https://github.com/dense-analysis/ale.git ~/.vim/pack/git-plugins/start/ale
```

Homepage:
https://github.com/dense-analysis/ale

airline
-------

Just a nice status bar

Installation:
```
mkdir -p ~/.vim/pack/dist/start
git clone https://github.com/vim-airline/vim-airline ~/.vim/pack/dist/start/vim-airline
```

Homepage:
https://github.com/vim-airline/vim-airline

airline themes
--------------

Taken from a separate repo

Installation"
```
git clone https://github.com/vim-airline/vim-airline-themes ~/.vim/pack/dist/start/vim-airline-themes
```

Homepage:
https://github.com/vim-airline/vim-airline-themes#vim-airline-themes--

gitgutter
---------

Shows what lines were changed against git repo

Installation:
```
mkdir -p ~/.vim/pack/airblade/start
cd ~/.vim/pack/airblade/start
git clone https://github.com/airblade/vim-gitgutter.git
vim -u NONE -c "helptags vim-gitgutter/doc" -c q
```

Homepage:
https://github.com/airblade/vim-gitgutter

Deps:
needs `git` to be installed

TODO: lots of customization are there

vimagit
-------

Like emacs `magit` - show git changes, stage them one by one, commit them

Works together with gitgutter

Installation:
```
mkdir -p ~/.vim/pack/git-plugins/start
git clone https://github.com/jreybert/vimagit ~/.vim/pack/git-plugins/start/vimagit
```

Homepage:
https://github.com/jreybert/vimagit

Usage:
:Magit

TODO: lots of configuration are there

