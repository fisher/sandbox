filetype plugin indent on
" show existing tab with 4 spaces width
set tabstop=4
" when indenting with '>', use 4 spaces width
set shiftwidth=4
" on pressing tab, insert 4 spaces
set expandtab
set vb
au BufReadPost *.spec.template set syntax=spec
syntax on

" these are to change the cursor shape in different modes
" (Insert, Replace, Command)
let &t_SI = "\<Esc>[6 q"
let &t_SR = "\<Esc>[2 q"
let &t_EI = "\<Esc>[4 q"
colorscheme desert

" set nonumbers to switch off line numbers
set number
highlight LineNr ctermfg=DarkGrey

""" taglist config
nmap <F8> :TlistToggle<CR>

""" ALE config
nmap <silent> <F4> <Plug>(ale_previous_wrap)
nmap <F5> <Plug>(ale_next_wrap)
let g:ale_sign_error = '!>'
let g:ale_sign_warning = '?>'
let g:ale_sign_column_always = 1
"highlight ALEWarning ctermbg=DarkMagenta
"see :help ale-highlights
" Set this. Airline will handle the rest.
" let g:airline#extensions#ale#enabled = 1

let g:ale_echo_msg_error_str = 'E'
let g:ale_echo_msg_warning_str = 'W'
let g:ale_echo_msg_format = '[%linter%](%severity%) %s'

let g:ale_floating_window_border = ['│', '─', '╭', '╮', '╯', '╰', '│', '─']

""" airline config
let g:airline#extensions#tabline#enabled = 1
"let g:airline#extensions#tabline#left_sep = ' '
"let g:airline#extensions#tabline#left_alt_sep = '|'
    
"possible values are: default jsformatter unique_tail unique_tail_improved
let g:airline#extensions#tabline#formatter = 'unique_tail_improved'

""" airline theme
"bubblegum sol base16_spacemacs papercolor
let g:airline_theme='sol'

""" gitgutter
" a little bit annoying if turned on
"let g:gitgutter_highlight_lines = 1

" this is much more apropriate alternative
let g:gitgutter_highlight_linenrs = 1

" this affects reloading gitgutter signs. Default is 4000 ms
set updatetime=1000

