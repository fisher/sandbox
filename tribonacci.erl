-module(tribonacci).

-export([tribonacci/1]).

tribonacci(1) -> 1;
tribonacci(2) -> 1;
tribonacci(3) -> 2;
tribonacci(N)
  when is_integer(N) andalso N > 3 ->
    tribonacci(N,{1,1,2}, 4).

tribonacci(N, {A1,A2,A3}, C)
  when C == N ->
    A1 + A2 + A3;
tribonacci(N, {A1, A2, A3}, C) ->
    tribonacci(N, {A2, A3, A1+A2+A3}, C+1).
