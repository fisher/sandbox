#!/usr/bin/env zsh
for i in {0..255} ; do
    printf "\x1b[38;5;${i}mcolour${i}\t\t\x1b[0m\x1b[48;5;${i}m   -- ${i} -- \x1b[38;5;0m        -- ${i} --     \x1b[0m\n"
done
