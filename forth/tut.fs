\ should work in gforth,
\ $ gforth tut.fs
\ interactively it works as well, in gforth prompt:
\ s" tut.fs" included
\ 7 stars
\ cr 5 triangle
\ cr 7 tower


: star ( -- ) 42 emit ; \ 42 is the ASCII code for *, line taken from example
: stars ( n -- ) assert( dup 0> ) 0 do star loop ; \ loop n times, 0 up to n-1

: square ( n -- )
    assert( dup 0> )
    dup 0 do
    dup stars cr
    loop drop ;

: triangle ( n -- )
    assert( dup 0> )
    1 + 1 do
    i stars cr
    loop ;

: tower ( n -- )
    dup
    1 - triangle
    square ;

