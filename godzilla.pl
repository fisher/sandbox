use strict;
use warnings;
use DDP;             # Data::Printer, provides 'p'

use Godzilla;
use Brontosaurus;

my $godzilla = Godzilla->new(name => "Masha", legs => 2);
my $bronto = Brontosaurus->new(name => "Petya");

p $bronto;

$godzilla->attack("Tokyo", "later");
$bronto->attack("Kyoto", "now");
