%%%-------------------------------------------------------------------
%%% @author Serge Rybalchenko <fisher@heim.in.ua>
%%% @copyright (C) 2021, Serge Rybalchenko
%%% @doc
%%%      playing with ARX RNG
%%% @end
%%% Created : 24 Apr 2021 by Serge Rybalchenko <fisher@heim.in.ua>
%%%-------------------------------------------------------------------
-module(arx).

%% API
-export([rotl32/2,rotr32/2,rotl64/2,rotr64/2,
         stream_add32/2,stream_add32/3]).
-export [f32b/1, f64b/1].

-compile([export_all,nowarn_export_all]).

-define(MAX32, 4294967296).

%% -record(
%%    state,
%%    {
%%     s0 = 0, s1 = 0, s2 = 0, s3 = 0,
%%     s4 = 0, s5 = 0, s6 = 0, s7 = 0,
%%     s8 = 0, s9 = 0, s10= 0, s11= 0,
%%     s12= 0, s13= 0, s14= 0, s15= 0
%%    }).

%%%===================================================================
%%% API
%%%===================================================================

%%--------------------------------------------------------------------
%% @doc
%% @spec
%% @end
%%--------------------------------------------------------------------
rotl32(V, N) when N > 32 ->
    rotl32(V, N rem 32);
rotl32(V, N) ->
    <<U:32, L:32>> = << (V bsl N):64/integer >>,
    U bor L.

rotl64(V, N) when N > 64 ->
    rotl64(V, N rem 64);
rotl64(V, N) ->
    <<U:64, L:64>> = << (V bsl N):128/integer >>,
    U bor L.

rotr32(V, N) when N > 32 ->
    rotr32(V, N rem 32);
rotr32(V, N) ->
    <<U:32, L:32>> = << (V bsl (32 -N)):64/integer >>,
    U bor L.

rotr64(V, N) when N > 64 ->
    rotr64(V, N rem 64);
rotr64(V, N) ->
    <<U:64, L:64>> = << (V bsl (64 -N)):128/integer >>,
    U bor L.

stream_add32(<<>>, _) -> <<>>;
stream_add32(<<Block:32, Rest/binary>>, Number) ->
    << (add32_1(Block, Number)):32, (stream_add32(Rest, Number))/binary >>.

stream_add32(<<>>, _, _) -> <<>>;
stream_add32(<<Block:32, Rest/binary>>, Number, Fun) ->
    << (Fun(Block, Number)):32, (stream_add32(Rest, Number))/binary >>.

add32(A,B) ->
    add32_1(A,B).

add32_1(A, B) ->
    (A + B) rem (1 bsl 32).

add32_2(A, B) ->
    (A + B) rem ?MAX32.

add32_3(A, B) ->
    <<I:32/integer>> = << (A + B):32 >>,
    I.

qr(S, A, B, C, D) ->    
    A1 = add32(element(A, S), element(B, S)),
    D1 = rotl32(element(D, S) bxor A1, 16),

    C1 = add32(element(C, S), D1),
    B1 = rotl32(element(B, S) bxor C1, 12),

    A2 = add32(A1, B1),
    D2 = rotl32(D1 bxor A2, 8),

    C2 = add32(C1, D2),
    B2 = rotl32(B1 bxor C2, 7),

    S0 = setelement(A, S, A2),
    S1 = setelement(B, S0, B2),
    S2 = setelement(C, S1, C2),
    setelement(D, S2, D2).

qr(A, B, C, D) ->
    A1 = add32(A, B),
    D1 = rotl32(D bxor A1, 16),

    C1 = add32(C, D1),
    B1 = rotl32(B bxor C1, 12),

    A2 = add32(A1, B1),
    D2 = rotl32(D1 bxor A2, 8),

    C2 = add32(C1, D2),
    B2 = rotl32(B1 bxor C2, 7),

    {A2, B2, C2, D2}.

sum_blocks(S1, S2) ->
    lists:foldl(
      fun(E, S) ->
              Sum = add32(element(E, S), element(E, S2)),
              setelement(E, S, Sum)
      end,
      S1,
      lists:seq(16,1,-1)).

block_round(S) ->
    Sequence =
        [{1, 5, 9,  13},
         {2, 6, 10, 14},
         {3, 7, 11, 15},
         {4, 8, 12, 16},
         {1, 6, 11, 16},
         {2, 7, 12, 13},
         {3, 8, 9,  14},
         {4, 5, 10, 15}],
    Fun = fun({A,B,C,D}, Acc) -> qr(Acc, A, B, C, D) end,
    lists:foldl(Fun, S, Sequence).

chacha20_block(Key, Counter, Nonce) ->
    S0 = init_state(Key, Counter, Nonce),
    fstate(S0), io:format("---------------~n", []),
    S1 = lists:foldl(fun(_,S) -> block_round(S) end, S0, lists:seq(1,10)),
    fstate(S1), io:format("===============~n", []),
    S2 = sum_blocks(S1,S0),
    fstate(S2), io:format("###############~n", []),
    S2 = test_result(),
    S3 = s_serialize(S2),
    fblob(S3),
    S3.

chacha20_nextblock(State) ->
    Counter = element(13, State) +1,
    S0 = setelement(13, State, Counter),
    S1 = lists:foldl(fun(_,S) -> block_round(S) end, S0, lists:seq(1,10)),
    S2 = sum_blocks(S1,S0),
    S3 = s_serialize(S2),
    {S0, S3}.                             

chacha20_encrypt(Key, Counter, Nonce, Plaintext) ->
    S0 = init_state(Key, Counter -1, Nonce),
    chacha20_chain(Plaintext, S0, <<>>).

chacha20_chain(B, S0, EncAcc) when size(B) >= 64 ->
    {S1, <<Keyblock:64/little-unit:8>>} = chacha20_nextblock(S0),
    fstate(S1),
    %%fblob(Keyblock),
    <<Plainblock:64/little-unit:8, Rest/binary>> = B,
    %% EncryptedBlock = xor_blocks(Plainblock, Keyblock, <<>>),
    %% EncryptedText = << EncryptedBlock/binary, EncAcc/binary >>,
    Encrypted = Plainblock bxor Keyblock,
    EncryptedText = << EncAcc/binary, Encrypted:64/little-unsigned-unit:8 >>,
    fblob(EncryptedText),
    chacha20_chain(Rest, S1, EncryptedText);
chacha20_chain(Last, S0, EncAcc) ->
    {S1, Keyblock} = chacha20_nextblock(S0),
    fstate(S1),
    EncryptedBlock = xor_blocks(Last, Keyblock, <<>>),
    << EncAcc/binary, EncryptedBlock/binary >>.

%% keyblock size GTE plainblock size
xor_blocks(<<P, PT/binary>>, <<K, KT/binary>>, Acc) ->
    xor_blocks(PT, KT, <<Acc/binary, (P bxor K)>>);
xor_blocks(<<>>, _, Acc) -> Acc.

%% RFC 8439
init_state(Key, Blockcount, Nonce)
  when is_binary(Key), is_binary(Nonce), Blockcount < 1 bsl 32 ->
    << K1:32/little, K2:32/little, K3:32/little, K4:32/little,
       K5:32/little, K6:32/little, K7:32/little, K8:32/little >> = Key,
    << N1:4/little-unsigned-unit:8, N2:4/little-unsigned-unit:8,
       N3:4/little-unsigned-unit:8 >> = Nonce,
    { 16#61707865, 16#3320646e, 16#79622d32, 16#6b206574,
      K1, K2, K3, K4, K5, K6, K7, K8,
      Blockcount, N1, N2, N3 }.

%%%===================================================================
%%% Internal functions
%%%===================================================================
f32b(I) ->
    io:format("~32.2.0B~n",[I]).
f64b(I) ->
    io:format("~64.2.0B~n",[I]).

frow(A,B,C,D) ->
    io:format("~8.16.0b  ~8.16.0b  ~8.16.0b  ~8.16.0b~n", [A,B,C,D]).

fstate(S) ->
    frow(element(1,S), element(2,S), element(3,S), element(4,S)),
    frow(element(5,S), element(6,S), element(7,S), element(8,S)),
    frow(element(9,S), element(10,S), element(11,S), element(12,S)),
    frow(element(13,S), element(14,S), element(15,S), element(16,S)).    

fblrow(<<B, T/binary>>, Counter) ->
    if Counter rem 4 == 0 -> io:format("  ~2.16.0b", [B]);
       true -> io:format(" ~2.16.0b", [B]) end,
    fblrow(T, Counter -1);
fblrow(_, 0) -> io:format("~n", []);
fblrow(<<>>, Counter) ->
    if Counter rem 4 == 0 -> io:format("  --", []);
       true -> io:format(" --", []) end,
    fblrow(<<>>, Counter -1).

fbquad(<<B, T/binary>>) ->
    io:format(" ~2.16.0b", [B]),
    fbquad(T);
fbquad(<<>>) -> io:put_chars(" ").

fb16(<<Q:4/binary-unit:8, T/binary>>) -> fbquad(Q), fb16(T);
fb16(Rem) -> fbquad(Rem).

show_printable(<<B, T/binary>>)
  when B > 31, B < 127 ->
    io:format("~c", [B]),
    show_printable(T);
show_printable(<<_, T/binary>>) ->
    io:put_chars("."),
    show_printable(T);
show_printable(_) ->
    io:format("~n", []).

pad16(B) when size(B) < 16 ->
    Mis = 16 - size(B),
    Pad = lists:foldl(
            fun(_,A)->[32|A]end," ",
            lists:seq(1,Mis *3 + Mis div 4)),
    io:put_chars(Pad);
pad16(_) -> ok.

fbrow(Offset, B) ->
    io:format("~4.16.0b ", [Offset]),
    fb16(B),
    pad16(B),
    show_printable(B).

fblob(B) when is_binary(B) ->
    fblob(B, 0).
fblob(<<B:16/binary-unit:8, T/binary>>, Offset) ->
    fbrow(Offset, B),
    fblob(T, Offset +16);
fblob(<<>>, _) ->
    io:format("~n", []);
fblob(B, Offset) when is_binary(B) ->
    fbrow(Offset, B).

s_serialize({A0,B0,C0,D0,A1,B1,C1,D1,A2,B2,C2,D2,A3,B3,C3,D3}) ->
    << A0:32/little, B0:32/little, C0:32/little, D0:32/little,
       A1:32/little, B1:32/little, C1:32/little, D1:32/little,
       A2:32/little, B2:32/little, C2:32/little, D2:32/little,
       A3:32/little, B3:32/little, C3:32/little, D3:32/little >>.

serialize(S) ->
    serialize(lists:reverse(tuple_to_list(S)), <<>>).
serialize([], Acc) ->
    Acc;
serialize([H|T], Acc) ->
    serialize(T, << H:4/little-unsigned-unit:8, Acc/binary >>).

pow(X, 0) when is_integer(X) -> 1;
pow(X, Y) when is_integer(X), is_integer(Y), Y > 0 ->
    pow_chain(X, pow_bits(Y, []), X).

pow_chain(_, [], Acc) -> Acc;
pow_chain(X, [0|Bits], Acc) -> pow_chain(X, Bits, Acc*Acc);
pow_chain(X, [_|Bits], Acc) -> pow_chain(X, Bits, Acc*Acc*X).

pow_bits(1, Acc) -> Acc;
pow_bits(Y, Acc) -> pow_bits(Y div 2, [Y rem 2 |Acc]).

power(_, 0) -> 1;
power(X, 1) when is_integer(X) -> X;
power(X, N) when is_integer(X), is_integer(N) ->
    B = power(X, N div 2),
    B * B * (case N rem 2 of 0 -> 1; _ -> X end).

qr_w(A, B, C, D) ->
    A1 = add32(A, B),
    D1 = D bxor A1,
    D2 = rotl32(D1, 16),
    
    C1 = add32(C, D2),
    B1 = B bxor C1,
    B2 = rotl32(B1, 12),
    
    A2 = add32(A1, B2),
    D3 = D2 bxor A2,
    D4 = rotl32(D3, 8),
    
    C2 = add32(C1, D4),
    B3 = B2 bxor C2,
    B4 = rotl32(B3, 7),
    
    {A2, B4, C2, D4}.

%%   Sample ChaCha State
sample_chacha_state() ->
    { 16#879531e0, 16#c5ecf37d, 16#516461b1, 16#c9a62f8a,
      16#44c20ef3, 16#3390af7f, 16#d9fc690b, 16#2a5f714c,
      16#53372767, 16#b00a5631, 16#974c541a, 16#359e9963,
      16#5c971061, 16#3d631689, 16#2098d9d6, 16#91dbd320 }.

test_key() ->
    list_to_binary(lists:seq(0,31)).
test_nonce() ->
    <<0,0,0,9,0,0,0,16#4a,0,0,0,0>>.
test_result() ->
     { 16#e4e7f110, 16#15593bd1, 16#1fdd0f50, 16#c47120a3,
       16#c7f4d1c7, 16#0368c033, 16#9aaa2204, 16#4e6cd4c3,
       16#466482d2, 16#09aa9f07, 16#05d7c214, 16#a2028bd9,
       16#d19c12b5, 16#b94e16de, 16#e883d0cb, 16#4e3c50a2 }.

%% following values (test_rfc_*) were taken from RFC 8439, chapter 2.4.2
test_rfc_key() ->
    list_to_binary(lists:seq(0,31)).

test_rfc_nonce() ->
    <<0,0,0,0,0,0,0,16#4a,0,0,0,0>>.

%%     Plaintext Sunscreen:
%% 000  4c 61 64 69 65 73 20 61 6e 64 20 47 65 6e 74 6c  Ladies and Gentl
%% 016  65 6d 65 6e 20 6f 66 20 74 68 65 20 63 6c 61 73  emen of the clas
%% 032  73 20 6f 66 20 27 39 39 3a 20 49 66 20 49 20 63  s of '99: If I c
%% 048  6f 75 6c 64 20 6f 66 66 65 72 20 79 6f 75 20 6f  ould offer you o
%% 064  6e 6c 79 20 6f 6e 65 20 74 69 70 20 66 6f 72 20  nly one tip for
%% 080  74 68 65 20 66 75 74 75 72 65 2c 20 73 75 6e 73  the future, suns
%% 096  63 72 65 65 6e 20 77 6f 75 6c 64 20 62 65 20 69  creen would be i
%% 112  74 2e                                            t.
test_rfc_plaintext() ->
    <<"Ladies and Gentlemen of the class of '99: If I could offer you o"
      "nly one tip for the future, sunscreen would be it.">>.

%% Ciphertext Sunscreen:
%% 000  6e 2e 35 9a 25 68 f9 80 41 ba 07 28 dd 0d 69 81  n.5.%h..A..(..i.
%% 016  e9 7e 7a ec 1d 43 60 c2 0a 27 af cc fd 9f ae 0b  .~z..C`..'......
%% 032  f9 1b 65 c5 52 47 33 ab 8f 59 3d ab cd 62 b3 57  ..e.RG3..Y=..b.W
%% 048  16 39 d6 24 e6 51 52 ab 8f 53 0c 35 9f 08 61 d8  .9.$.QR..S.5..a.
%% 064  07 ca 0d bf 50 0d 6a 61 56 a3 8e 08 8a 22 b6 5e  ....P.jaV....".^
%% 080  52 bc 51 4d 16 cc f8 06 81 8c e9 1a b7 79 37 36  R.QM.........y76
%% 096  5a f9 0b bf 74 a3 5b e6 b4 0b 8e ed f2 78 5e 42  Z...t.[......x^B
%% 112  87 4d                                            .M
test_rfc_ciphertext() ->
    <<
      16#6e,16#2e,16#35,16#9a,16#25,16#68,16#f9,16#80
     ,16#41,16#ba,16#07,16#28,16#dd,16#0d,16#69,16#81
     ,16#e9,16#7e,16#7a,16#ec,16#1d,16#43,16#60,16#c2
     ,16#0a,16#27,16#af,16#cc,16#fd,16#9f,16#ae,16#0b
     ,16#f9,16#1b,16#65,16#c5,16#52,16#47,16#33,16#ab
     ,16#8f,16#59,16#3d,16#ab,16#cd,16#62,16#b3,16#57
     ,16#16,16#39,16#d6,16#24,16#e6,16#51,16#52,16#ab
     ,16#8f,16#53,16#0c,16#35,16#9f,16#08,16#61,16#d8
     ,16#07,16#ca,16#0d,16#bf,16#50,16#0d,16#6a,16#61
     ,16#56,16#a3,16#8e,16#08,16#8a,16#22,16#b6,16#5e
     ,16#52,16#bc,16#51,16#4d,16#16,16#cc,16#f8,16#06
     ,16#81,16#8c,16#e9,16#1a,16#b7,16#79,16#37,16#36
     ,16#5a,16#f9,16#0b,16#bf,16#74,16#a3,16#5b,16#e6
     ,16#b4,16#0b,16#8e,16#ed,16#f2,16#78,16#5e,16#42
     ,16#87,16#4d
    >>.
test_rfc_run() ->
    test_rfc_ciphertext() ==
        chacha20_encrypt(
          test_rfc_key(), 1, test_rfc_nonce(), test_rfc_plaintext()).

%% found in the examples for java implementation
test_short_key() ->
    << 16#ee416df8b5154a4ac48f3930fcfa53ef7f677c8fd7cd093f1328eedfd831db1a:32/big-unsigned-unit:8 >>.
test_short_nonce() ->
    << 16#9806308f4d1732d2d39beaba:12/big-unit:8 >>.
test_short_plaintext() ->
    << 16#4a617661202620436861436861323020656e6372797074696f6e206578616d706c652e:35/big-unsigned-unit:8 >>.
test_short_encrypted() ->
    << 16#2149db2c32bf82f9e8dc0a709d8c15d5a22eb79d5f692e04f070d46cc7e264631f85e0:35/big-unsigned-unit:8 >>.
test_short_run() ->
    Enc = chacha20_encrypt(test_short_key(),1,test_short_nonce(),test_short_plaintext()),
    Enc == test_short_encrypted().
