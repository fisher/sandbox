package Brontosaurus;
use Moo;
extends 'Godzilla';

# after, before, around - modifiers
# before for validation
# after for logging
# around for replacing (possible to call original method via $orig->($self, $arg))

around move_to => sub {
    my ($orig, $self, $where) = @_;
    print $self->name." is crowling to $where\n";
};

1;
