package Godzilla;
use Moo;

has legs => (is => 'ro', default => sub { 4 });   # private attribute
has name => (is => 'rw');   # public
has color => (is => 'ro', builder => '_build_color'); # builder for structures

sub breath_fire { my ($ss) = @_; print $ss->name." breathing fire!\n" }
sub move_to { my ($ss, $where) = @_; print $ss->name." is walking to $where\n" }

# Perlism: methods that start with _ are private
sub _build_color { "green" }

sub wait_until {
    my ($self, $when) = @_;
    print $self->name." is waiting for $when\n" }

sub attack {
    my ($self, $where, $when) = @_;
    $self->wait_until($when);
    $self->move_to($where);
    $self->breath_fire();
}

# destructor
sub DEMOLISH {
    my ($this) = @_;
    print $this->name.": I'm dying!\n";
}

1;

