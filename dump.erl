%%%-------------------------------------------------------------------
%%% @author Serge Rybalchenko <fisher@heim.in.ua>
%%% @copyright (C) 2021, Serge Rybalchenko
%%% @doc
%%%      hex dump binaries
%%% @end
%%% Created : 11 Jul 2021 by Serge Rybalchenko <fisher@heim.in.ua>
%%%-------------------------------------------------------------------
-module(dump).

%% API
-export([
          hd/1               % classic hexdump
         ,pdp11/1, pdp11/2   % old 'dump' command from RT-11 OS
         %hexyl/1            % modny molodezny
         ,rad50/1            % RADIX-50, 16bit version (MOD40)
        ]).

%%%===================================================================
%%% API
%%%===================================================================

%%--------------------------------------------------------------------
%% @doc
%%      Classic hexdump
%% @end
%%--------------------------------------------------------------------
-spec hd (Binary :: binary()) -> ok.
hd(B) when is_binary(B) -> hd(B, 0).

%%--------------------------------------------------------------------
%% @doc
%%      PDP11 'dump' style
%% @end
%%--------------------------------------------------------------------
-spec pdp11 (Binary :: binary()) -> ok.
pdp11(B) -> pdp11(B, #{}).

%% DK:hello.obj
%% BLOCK NUMBER  000000
%% 000/ 000001 000046 000001 031324 046530 000000 000000 127401 *..&...T2XM...../*
%% 020/ 007624 002504 000000 000000 000000 002440 000025 000000 *..D....... .....*
%% 040/ 000000 001450 000000 000454 003000 001000 173400 000001 *..(...,......w..*
%% 060/ 000016 000004 000007 000000 000000 000000 000746 016400 *............f...*
%% 100/ 001400 000000 140000 004025 164400 164210 044210 046105 *.....@...i.h.HEL*
%% 120/ 047514 020054 047527 046122 000104 000731 005000 002000 *LO, WORLD.Y.....*
%% 140/ 000400 004006 161000 000001 000006 000006 000363 000000 *.....b......s...*
%% 160/ 000000 000000 000000 000000 000000 000000 000000 000000 *................*

%% .dump /terminal/bytes hello.obj

%% DK:hello.obj
%% BLOCK NUMBER  000000
%% 000/ 001 000 046 000 001 000 324 062 130 115 000 000 000 000 001 257
%%      .   .   &   .   .   .   T   2   X   M   .   .   .   .   .   /
%% 020/ 224 017 104 005 000 000 000 000 000 000 040 005 025 000 000 000
%%      .   .   D   .   .   .   .   .   .   .       .   .   .   .   .
%% 040/ 000 000 050 003 000 000 054 001 000 006 000 002 000 367 001 000
%%      .   .   (   .   .   .   ,   .   .   .   .   .   .   w   .   .
%% 060/ 016 000 004 000 007 000 000 000 000 000 000 000 346 001 000 035
%%      .   .   .   .   .   .   .   .   .   .   .   .   f   .   .   .
%% 100/ 000 003 000 000 000 300 025 010 000 351 210 350 210 110 105 114
%%      .   .   .   .   .   @   .   .   .   i   .   h   .   H   E   L
%% 120/ 114 117 054 040 127 117 122 114 104 000 331 001 000 012 000 004
%%      L   O   ,       W   O   R   L   D   .   Y   .   .   .   .   .
%% 140/ 000 001 006 010 000 342 001 000 006 000 006 000 363 000 000 000
%%      .   .   .   .   .   b   .   .   .   .   .   .   s   .   .   .
%% 160/ 000 000 000 000 000 000 000 000 000 000 000 000 000 000 000 000
%%      .   .   .   .   .   .   .   .   .   .   .   .   .   .   .   .

%% .dump/terminal/bytes/rad50 hello.obj
%% DK:hello.obj
%% BLOCK NUMBER  000000
%% 000/ 001 000 046 000 001 000 324 062 130 115 000 000 000 000 001 257
%%      .   .   &   .   .   .   T   2   X   M   .   .   .   .   .   /
%%        A       8       A     HEL     LO                      . A
%% 020/ 224 017 104 005 000 000 000 000 000 000 040 005 025 000 000 000
%%      .   .   D   .   .   .   .   .   .   .       .   .   .   .   .
%%      BS.      3.                              22       U        
%% 040/ 000 000 050 003 000 000 054 001 000 006 000 002 000 367 001 000
%%      .   .   (   .   .   .   ,   .   .   .   .   .   .   w   .   .
%%               TH              GT      8P      L2     9T2       A
%% 060/ 016 000 004 000 007 000 000 000 000 000 000 000 346 001 000 035
%%      .   .   .   .   .   .   .   .   .   .   .   .   f   .   .   .
%%        N       D       G                              LF     DYX

%% .dump/termi/rad50 hello.obj


%% DK:hello.obj
%% BLOCK NUMBER  000000
%% 000/ 000001 000046 000001 031324 046530 000000 000000 127401 *..&...T2XM...../*
%%        A      8      A    HEL    LO                   . A
%% 020/ 007624 002504 000000 000000 000000 002440 000025 000000 *..D....... .....*
%%      BS.     3.                          22      U       
%% 040/ 000000 001450 000000 000454 003000 001000 173400 000001 *..(...,......w..*
%%              TH            GT     8P     L2    9T2      A
%% 060/ 000016 000004 000007 000000 000000 000000 000746 016400 *............f...*
%%        N      D      G                          LF    DYX
%% 100/ 001400 000000 140000 004025 164400 164210 044210 046105 *.....@...i.h.HEL*
%%       SH           0.2    AK/    7KH    7HH    KXH    LHE
%% 120/ 047514 020054 047527 046122 000104 000731 005000 002000 *LO, WORLD.Y.....*
%%      L$T    EE6    L$1    LHR     A.     K3    AX      YX
%% 140/ 000400 004006 161000 000001 000006 000006 000363 000000 *.....b......s...*
%%       FP    AKN    6FP      A      F      F     FC       

-spec pdp11 (Binary :: binary(), Options :: map()) -> ok.
pdp11(B, Options)
  when is_binary(B),
       is_map(Options) ->
    pdp11_dump(B, 0).

%% cannot split by bits, this is mod 40
%% also note PDP-endian, 32-bit integer 0x0a0b0c0d is
%% (increasing addresses -->)
%% ...|0b|0a|0d|0c|...
%% (i.e. little-endian within 16-bit words and big endian within 2 word 32bit value)
rad50(<<W:1/integer-unit:16, T/binary>>) ->
    C = W rem 40, W1 = W div 40,
    B = W1 rem 40, W2 = W1 div 40,
    A = W2 rem 40,
    A0 = rad50_(A), B0 = rad50_(B), C0 = rad50_(C),
    %% io:format("~6.2.0b-~6.2.0b-~6.2.0b '~c~c~c'", [A,B,C,A0,B0,C0]),
    io:format("~c~c~c-", [A0,B0,C0]),
    rad50(T);
rad50(<<Byte:1/integer-unit:8>>) ->
    C = Byte rem 40, B = Byte div 40,
    io:format("?~c~c-", [B,C]);
rad50(Other) ->
    io:format("~nRemainder: '~p'",[Other]).

rad50_(I) when I >0 andalso I <27 ->
    $A+I-1;
rad50_(I) when I >29 andalso I <40 ->
    $0+I-30;
rad50_(0) -> 32;
rad50_(27) -> $$;
rad50_(28) -> $.;
rad50_(29) -> $%.

%%%===================================================================
%%% Internal functions
%%%===================================================================

pdp11_dump(<<B:16/binary-unit:8, T/binary>>, Offset) ->
    io:format("~3.8.0b/ ", [Offset]),
    pdp11_row(B),
    pdp11_pad(B),
    pdp11_printable(B,16),
    pdp11_dump(T, Offset +16);
pdp11_dump(<<>>, _) -> ok;
pdp11_dump(B, Offset) when is_binary(B) ->
    io:format("~3.8.0b/ ", [Offset]),
    pdp11_row(B),
    pdp11_pad(B),
    pdp11_printable(B,16).

pdp11_printable(<<B, T/binary>>, Left) ->
    case B > 31 andalso B < 127 of
        true ->
            io:format("~c", [B]);
        _ ->
            io:put_chars(".")
    end,
    pdp11_printable(T, Left -1);
pdp11_printable(_, Left) when Left >0 ->
    io:put_chars("."),
    pdp11_printable(<<>>, Left -1);
pdp11_printable(_, 0) ->
    io:put_chars(<<"*", 10>>).

%% close_row(Size) ->
%%     Missing = 16 - Size,
%%     Pad = << <<$.>> || _<-lists:seq(1,Missing) >>,
%%     io:put_chars(<<Pad/binary, "*", 10>>).

pdp11_row(<<W:1/integer-little-unit:16, T/binary>>) ->
    io:format("~6.8.0b ", [W]),
    pdp11_row(T);
pdp11_row(<<LastByte:1/integer-unit:8>>) ->
    io:format("~6.8.0b ", [LastByte]);
pdp11_row(<<>>) -> ok.

pdp11_pad(B) when size(B) < 15 ->
    Mis = 8 - (size(B) +1) div 2,
    Pad = lists:foldl(
            fun(_,A)-><<"------ ",A/binary>>end,<<>>,
            lists:seq(1,Mis)),
    io:put_chars(<<Pad/binary, "*">>);
pdp11_pad(_) ->
    io:put_chars("*").

hd(<<B:16/binary-unit:8, T/binary>>, Offset) ->
    fbrow(Offset, B),
    hd(T, Offset +16);
hd(<<>>, _) ->
    io:format("~n", []);
hd(B, Offset) when is_binary(B) ->
    fbrow(Offset, B).

fbrow(Offset, B) ->
    io:format("~4.16.0b ", [Offset]),
    fb16(B),
    pad16(B),
    show_printable(B).

fb16(<<Q:4/binary-unit:8, T/binary>>) -> fbquad(Q), fb16(T);
fb16(Rem) -> fbquad(Rem).

fbquad(<<B, T/binary>>) ->
    io:format(" ~2.16.0b", [B]),
    fbquad(T);
fbquad(<<>>) -> io:put_chars(" ").

show_printable(<<B, T/binary>>)
  when B > 31, B < 127 ->
    io:format("~c", [B]),
    show_printable(T);
show_printable(<<_, T/binary>>) ->
    io:put_chars("."),
    show_printable(T);
show_printable(_) ->
    io:format("~n", []).

pad16(B) when size(B) < 16 ->
    Mis = 16 - size(B),
    Pad = lists:foldl(
            fun(_,A)->[32|A]end," ",
            lists:seq(1,Mis *3 + Mis div 4)),
    io:put_chars(Pad);
pad16(_) -> ok.
