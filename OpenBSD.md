OpenBSD notes
=============

Why
---

I've been a long time OpenBSD user.

This file is a reminder for myself why I'm not anymore.

Why not
-------

Here's why

* no bluetooth

They ripped off all the bt proto from the kernel. There is no way to listen to music via
bluetooth phones at all. You're suggested to use wired ears.

* no ext4 filesystem support

There is no any way to mount a filesystem other than bsd. This blows my mind, because
the ext4fs is opensourced, documented and can be easily adopted to mount ext drives.
This wouldn't be so complicated as BT proto. But.
They don't want this, looks like a political issue.

* To put it short

Linux is an evolution. OpenBSD is a creation.

What I found not to be so critical to have
------------------------------------------

* weird stupid problems which were resolved ages ago in Linux - they are generally ok
* No skype
* no slack
* no telegram (update: telegram works in browser as a webapp)
* every fucking app in the internet just doesn't know about the very existence of OpenBSD
* Backspace-Delete BS.
** xterm in obsd by default doesn't distinguish between those. `XTerm*deleteIsDEL: false`
** now make sure shell and others know how to deal with it. `bindkey "^[[3~" delete-char`
** some terminal applications behave weird with defaul `^?`, so `XTerm.ttyModes: erase ^H`
** check `stty all` what `erase` char is
* fucking `pledge` breaks every other app like firefox or audacious
** for firefox, `echo "file:///home/fisher/Firefox" >.config/gtk-3.0/bookmarks` fix

